comando: sudo journalctl -b | curl -F 'file=@-' 0x0.st


###Como usar o Journalctl para visualizar e manipular registros do systemd

Introdução
Algumas das vantagens mais interessantes do systemd são aquelas envolvidas com o registro de processos e do sistema. Ao usar outros sistemas, os registros ficam geralmente dispersos, e são manuseados por daemons e processos diferentes. Isso torna a interpretação deles bastante difícil quando englobam vários aplicativos. O systemd tenta resolver esses problemas fornecendo uma solução de gerenciamento centralizada para registrar todos os processos do kernel e do userland. O sistema que coleta e gerencia esses registros é conhecido como journal (diário).

O diário é implementado com o daemon journald, que manuseia todas as mensagens produzidas pelo kernel, initrd, serviços etc. Neste guia, vamos discutir como utilizar o utilitário journalctl, que pode ser usado para acessar e manipular os dados mantidos dentro do diário.

Ideal geral
Um dos motivos da existência do diário do systemd é o de centralizar o gerenciamento de registros independentemente de onde as mensagens estão sendo originadas. Como uma grande parte do processo de inicialização do sistema e gerenciamento de serviços é manuseada pelo processo systemd, faz sentido padronizar a maneira como os registros são coletados e acessados. O daemon journald coleta dados de todas as fontes disponíveis e os armazena em um formato binário para uma manipulação fácil e dinâmica.

Isso nos dá várias vantagens significativas. Ao interagir com os dados usando um único utilitário, os administradores são capazes de exibir dinamicamente dados de registro de acordo com suas necessidades. Isso pode ser algo simples como visualizar os dados de inicialização de três inicializações de sistema atrás ou combinar as entradas de registro sequencialmente de dois serviços relacionados para consertar um problema de comunicação.

Armazenar os dados de registro em um formato binário também faz com que eles possam ser exibidos em formatos de saída arbitrários, dependendo da sua necessidade naquele momento. Por exemplo, para o gerenciamento de registros diários, você pode estar acostumado a visualizar os registros no formato syslog padrão. No entanto, se você quiser representar interrupções de serviço em gráficos mais tarde, é possível gerar um objeto JSON para cada entrada para que seja consumível pelo seu serviço de criação de gráficos. Como os dados não são escritos no disco em texto simples, nenhuma conversão é necessária quando houver a demanda por um formato diferente.

O diário do systemd pode ser usado com uma implementação do syslog existente, ou pode substituir a funcionalidade syslog, dependendo das suas necessidades. Embora o diário do systemd atenda a maioria das necessidades de registro de administrador, ele também pode complementar mecanismos de registro existentes. Por exemplo, pode ser que você tenha um servidor syslog centralizado que usa para compilar dados de vários servidores, mas também queira intercalar os registros de vários serviços em um único sistema com o diário do systemd. É possível fazer as duas coisas combinando essas tecnologias.

Configurando o horário do sistema
Um dos benefícios do uso de um diário binário para registro é a capacidade de visualizar registros em UTC ou em seu horário local à vontade. Por padrão, o systemd irá exibir resultados no horário local.

Por conta disso, antes de começarmos com o diário, vamos garantir que o fuso horário esteja configurado corretamente. O pacote do systemd vem com uma ferramenta chamada timedatectl que pode ajudar com isso.

Primeiramente, consulte quais fusos horários estão disponíveis com a opção list-timezones:

timedatectl list-timezones
Isso irá listar os fusos horários disponíveis no seu sistema. Depois de encontrar aquele que corresponde ao local do seu servidor, defina-o usando a opção set-timezone:

sudo timedatectl set-timezone zone
Para garantir que sua máquina esteja usando o horário correto neste momento, use o comando timedatectl sozinho, ou com a opção status. O resultado será o mesmo:

timedatectl status
      Local time: Thu 2015-02-05 14:08:06 EST
  Universal time: Thu 2015-02-05 19:08:06 UTC
        RTC time: Thu 2015-02-05 19:08:06
       Time zone: America/New_York (EST, -0500)
     NTP enabled: no
NTP synchronized: no
 RTC in local TZ: no
      DST active: n/a
A primeira linha deve exibir o horário correto.

Visualização básica de registros
Para ver os registros que o daemon do journald coletou, use o comando journalctl.

Quando usado sozinho, todas as entradas no diário que estão no sistema serão exibidas dentro de um pager (geralmente less (menos)) para você navegar. As entradas mais antigas estarão no topo:

journalctl
-- Logs begin at Tue 2015-02-03 21:48:52 UTC, end at Tue 2015-02-03 22:29:38 UTC. --
Feb 03 21:48:52 localhost.localdomain systemd-journal[243]: Runtime journal is using 6.2M (max allowed 49.
Feb 03 21:48:52 localhost.localdomain systemd-journal[243]: Runtime journal is using 6.2M (max allowed 49.
Feb 03 21:48:52 localhost.localdomain systemd-journald[139]: Received SIGTERM from PID 1 (systemd).
Feb 03 21:48:52 localhost.localdomain kernel: audit: type=1404 audit(1423000132.274:2): enforcing=1 old_en
Feb 03 21:48:52 localhost.localdomain kernel: SELinux: 2048 avtab hash slots, 104131 rules.
Feb 03 21:48:52 localhost.localdomain kernel: SELinux: 2048 avtab hash slots, 104131 rules.
Feb 03 21:48:52 localhost.localdomain kernel: input: ImExPS/2 Generic Explorer Mouse as /devices/platform/
Feb 03 21:48:52 localhost.localdomain kernel: SELinux:  8 users, 102 roles, 4976 types, 294 bools, 1 sens,
Feb 03 21:48:52 localhost.localdomain kernel: SELinux:  83 classes, 104131 rules

. . .
É provável que você tenha páginas e páginas de dados para percorrer, que podem ser dezenas ou centenas de milhares de linhas se o systemd estiver em seu sistema há bastante tempo. Isso demonstra a quantidade de dados que está disponível no banco de dados do diário.

O formato será conhecido por aqueles que estão acostumados com o registro padrão do syslog. No entanto, este processo coleta dados de mais fontes do que as implementações tradicionais do syslog são capazes de fazer. Ele inclui registros do processo de inicialização inicial, do kernel, do initrd e do erro padrão de aplicativo e mais. Todos eles estão disponíveis no diário.

Note que todos os carimbos de data/hora estão sendo exibidos no horário local. Isso está disponível para toda entrada de registro agora que temos nosso horário local configurado corretamente em nosso sistema. Todos os registros são exibidos usando essas novas informações.

Se quiser exibir os carimbos de data/hora em UTC, use o sinalizador --utc:

journalctl --utc
Filtrar o diário pela hora
Embora ter acesso a uma grande coleção de dados seja definitivamente útil, essa grande quantidade de informações pode ser difícil ou impossível de ser inspecionada e processada mentalmente. Por conta disso, uma das características mais importantes do journalctl é suas opções de filtragem.

Exibir registros da inicialização atual
A opção mais básica existente que pode ser usada diariamente, é o sinalizador -b. Ele irá mostrar todas as entradas do diário que foram coletadas desde a reinicialização mais recente.

journalctl -b
Isso ajudará você a identificar e gerenciar informações pertinentes ao seu ambiente atual.

Nos casos em que você não estiver usando esse recurso e estiver exibindo mais de um dia de inicializações, verá que o journalctl insere uma linha que se parece com esta, sempre que o sistema foi desligado:

. . .

-- Reboot --

. . .
Isso pode ser usado para ajudar a separar as informações em sessões de inicialização de maneira lógica.

Inicializações passadas
Embora seja comum querer exibir as informações da inicialização atual, certamente existem momentos em que as inicializações passadas também podem ser úteis. O diário pode salvar informações de várias inicializações anteriores, de modo que o journalctl pode ser usado para exibir essas informações facilmente.

Algumas distribuições habilitam o salvamento de informações de inicializações anteriores por padrão, enquanto outras desativam esse recurso. Para habilitar as informações de inicialização persistentes, crie o diretório para armazenar o diário digitando:

sudo mkdir -p /var/log/journal
Ou edite o arquivo de configuração do diário:

sudo nano /etc/systemd/journald.conf
Na seção [Journal], defina a opção Storage= como “persistent” para habilitar o registro persistente:

/etc/systemd/journald.conf
. . .
[Journal]
Storage=persistent
Quando o salvamento de inicializações anteriores está habilitado no seu servidor, o journalctl fornece alguns comandos para ajudar a trabalhar com as inicializações como uma unidade de divisão. Para ver as inicializações das quais o journald tem conhecimento, use a opção --list-boots com o journalctl:

journalctl --list-boots
-2 caf0524a1d394ce0bdbcff75b94444fe Tue 2015-02-03 21:48:52 UTC—Tue 2015-02-03 22:17:00 UTC
-1 13883d180dc0420db0abcb5fa26d6198 Tue 2015-02-03 22:17:03 UTC—Tue 2015-02-03 22:19:08 UTC
 0 bed718b17a73415fade0e4e7f4bea609 Tue 2015-02-03 22:19:12 UTC—Tue 2015-02-03 23:01:01 UTC
Isso irá exibir uma linha para cada inicialização. A primeira coluna é o deslocamento da inicialização em relação à atual que pode ser usado para identificá-la com facilidade com o journalctl. Se precisar de uma referência absoluta, o ID de inicialização está na segunda coluna. É possível identificar o tempo da sessão com as duas especificações de data/hora no final da linha.

Para exibir informações dessas inicializações, use as informações da primeira ou segunda coluna.

Por exemplo, para ver o diário da inicialização anterior, use o ponteiro relativo -1 com o sinalizador -b:

journalctl -b -1
Também é possível usar o ID de inicialização para retornar os dados de uma inicialização específica:

journalctl -b caf0524a1d394ce0bdbcff75b94444fe
Intervalos de tempo
Embora a visualização de entradas de registro com base na inicialização seja incrivelmente útil, muitas vezes é desejável solicitar intervalos de tempo que não se alinham com as inicializações do sistema. Isso pode ser especialmente válido ao lidar com servidores de execução prolongada com um tempo de atividade significativo.

É possível filtrar por limites arbitrários de data/hora usando as opções --since e --until, que restringem as entradas exibidas a aquelas que sucedem ou antecedem um determinado tempo, respectivamente.

Os valores de data/hora podem ser usados em uma variedade de formatos. Para valores de data/hora absolutos, use o seguinte formato:

YYYY-MM-DD HH:MM:SS
Por exemplo, podemos ver todas as entradas desde 10 de janeiro de 2015 às 5:15 PM digitando:

journalctl --since "2015-01-10 17:15:00"
Se algum componente do formato acima for deixado de fora, o padrão será aplicado. Por exemplo, se a data for omitida, a data atual será empregada. Se o componente de hora estiver faltando, “00:00:00” (meia-noite) será utilizado. O campo de segundos também pode ser deixado de fora e o padrão “00” é empregado:

journalctl --since "2015-01-10" --until "2015-01-11 03:00"
O diário também compreende alguns valores relativos e seus atalhos nomeados. Por exemplo, é possível usar as palavras “yesterday” (ontem), “today” (hoje), “tomorrow” (amanhã) ou “now” (agora). É possível criar datas/horas relativas prefixando “-” ou “+” a um valor numerado ou usando palavras como “ago” (atrás) em uma construção de sentenças.

Para obter os dados de ontem, pode-se digitar:

journalctl --since yesterday
Se tiver recebido relatórios de uma interrupção de serviço iniciada às 9:00 AM que durou até uma hora atrás, você pode digitar:

journalctl --since 09:00 --until "1 hour ago"
Como se vê, é relativamente fácil definir intervalos flexíveis de tempo para filtrar as entradas que você deseja visualizar.

Filtrar por interesse de mensagens
Aprendemos acima algumas maneiras de filtrar os dados do diário usando restrições de tempo. Nesta seção, vamos discutir como filtrar com base em qual serviço ou componente você está interessado. O diário do systemd oferece diversas maneiras de fazer isso.

Por unidade
Talvez a maneira mais útil de filtrar seja pela unidade na qual você está interessado. Podemos usar a opção -u para filtrar dessa maneira.

Por exemplo, para ver todos os registros de uma unidade Nginx em nosso sistema, digitamos:

journalctl -u nginx.service
Normalmente, também seria interessante filtrar pela data/hora para exibir as linhas nas quais você está interessado. Por exemplo, para verificar como o serviço está funcionando hoje, digite:

journalctl -u nginx.service --since today
Esse tipo de foco torna-se extremamente útil quando se aproveita da capacidade do diário de intercalar os registros de várias unidades. Por exemplo, se seu processo Nginx estiver conectado a uma unidade PHP-FPM para processar conteúdo dinâmico, é possível fundir as entradas de ambos em ordem cronológica especificando ambas as unidades:

journalctl -u nginx.service -u php-fpm.service --since today
Isso pode facilitar a detecção de interações entre diferentes programas e sistemas de depuração, em vez de processos individuais.

Por processo, usuário ou ID de grupo
Alguns serviços geram uma variedade de processos filhos para funcionar. Se você tiver pesquisado o PID exato do processo em que está interessado, também é possível filtrar por ele.

Para fazer isso, especificamos o campo _PID. Por exemplo, se o PID em que estivermos interessados for 8088, digitamos:

journalctl _PID=8088
Em outros momentos, pode ser desejável exibir todas as entradas registradas a partir de um usuário ou grupo específico. Isso pode ser feito com os filtros _UID ou _GID. Por exemplo, se seu servidor Web estiver sendo executado sob o usuário www-data, é possível encontrar o ID do usuário digitando:

id -u www-data
33
Depois disso, use o ID retornado para filtrar os resultados do diário:

journalctl _UID=33 --since today
O diário do systemd possui muitos campos que podem ser usados para a filtragem. Alguns deles são passados do processo que está sendo registrado e alguns são aplicados pelo journald usando informações que ele coleta do sistema no momento do registro.

O sublinhado inicial indica que o campo _PID pertence ao segundo tipo. O diário registra e classifica automaticamente o PID do processo que está sendo registrado para a filtragem posterior. É possível descobrir todos os campos de diário disponíveis digitando:

man systemd.journal-fields
Vamos discutir alguns deles neste guia. Por enquanto, vamos analisar mais uma opção útil relacionada com a filtragem por esses campos. A opção -F pode ser usada para mostrar todos os valores disponíveis para um campo de diário específico.

Por exemplo, para ver quais entradas para IDs de grupo o diário do systemd possui, digite:

journalctl -F _GID
32
99
102
133
81
84
100
0
124
87
Isso irá mostrar todos os valores que o diário armazenou para o campo ID de grupo. Isso pode ajudar a construir seus filtros.

Por caminho de componente
Também podemos filtrar fornecendo um caminho de pesquisa.

Se o caminho levar a um executável, o journalctl irá exibir todas as entradas relacionadas ao executável em questão. Por exemplo, para encontrar essas entradas que estão relacionadas ao executável bash, digite:

journalctl /usr/bin/bash
Normalmente, se uma unidade estiver disponível para o executável, esse método é mais organizado e fornece informações mais completas (entradas de processos filhos associados, etc). Às vezes, no entanto, isso não é possível.

Exibir mensagens de kernel
As mensagens de kernel, que são aquelas geralmente encontradas na saída do dmesg, também podem ser recuperadas do diário.

Para exibir apenas essas mensagens, podemos adicionar os sinalizadores -k ou --dmesg ao nosso comando:

journalctl -k
Por padrão, isso irá exibir as mensagens do kernel da inicialização atual. É possível especificar uma outra inicialização usando os sinalizadores de seleção de inicialização discutidos anteriormente. Por exemplo, para obter as mensagens de cinco inicializações atrás, digite:

journalctl -k -b -5
Por prioridade
Um filtro no qual os administradores de sistema estão geralmente interessados é a prioridade de mensagem. Embora seja muitas vezes útil registrar informações em um nível bastante detalhado, ao resumir as informações disponíveis, o registro de baixa prioridade pode ser confuso.

É possível usar o journalctl para exibir apenas mensagens de uma prioridade especificada ou superior usando a opção -p. Isso permite filtrar mensagens com níveis de prioridade inferiores.

Por exemplo, para mostrar apenas entradas registradas no nível de erro ou acima, digite:

journalctl -p err -b
Isso irá exibir todas as mensagens marcadas como erro, crítico, alerta ou emergência. O diário implementa os níveis de mensagem padrão do syslog. É possível usar o nome da prioridade ou seu valor numérico correspondente. As prioridades, ordenadas da maior para a menor, são:

0: emerg
1: alert
2: crit
3: err
4: warning
5: notice
6: info
7: debug
Os números ou nomes acima podem ser usados de maneira intercambiável com a -p opção. A seleção de uma prioridade irá exibir mensagens marcadas no nível especificado e acima.

Modificar a exibição do diário
Acima, demonstramos a seleção de entradas através da filtragem. No entanto, existem outras maneiras com as quais podemos modificar o resultado. Podemos ajustar a exibição do journalctl para atender diversas necessidades.

Truncar ou expandir o resultado
Podemos ajustar como o journalctl exibe os dados, dizendo-lhe para reduzir ou expandir o resultado.

Por padrão, o journalctl irá mostrar a entrada inteira no pager, permitindo que as entradas se expandam à direita da tela. Essa informação pode ser acessada pressionando a tecla de seta para a direita.

Se preferir o resultado truncado, inserindo reticências onde as informações foram removidas, use a opção --no-full:

journalctl --no-full
. . .

Feb 04 20:54:13 journalme sshd[937]: Failed password for root from 83.234.207.60...h2
Feb 04 20:54:13 journalme sshd[937]: Connection closed by 83.234.207.60 [preauth]
Feb 04 20:54:13 journalme sshd[937]: PAM 2 more authentication failures; logname...ot
Também é possível ir na direção oposta e dizer ao journalctl para exibir todas as suas informações, mesmo se forem incluídos caracteres não imprimíveis. Podemos fazer isso com o sinalizador -a:

journalctl -a
Resultados em formato padrão
Por padrão, o journalctl exibe o resultado em um pager para um consumo mais simples. No entanto, se você estiver planejando processar os dados com ferramentas de manipulação de texto, irá querer ser capaz de gerar resultados no formato padrão.

Faça isso com a opção --no-pager:

journalctl --no-pager
Isso pode ser canalizado imediatamente em um utilitário de processamento ou redirecionado para um arquivo no disco, dependendo das suas necessidades.

Formatos de saída
Se você estiver processando entradas de diário, como mencionado acima, provavelmente será mais fácil analisar os dados se eles estiverem em um formato mais consumível. Felizmente, o diário pode ser exibido em uma variedade de formatos conforme a necessidade. Faça isso usando a opção -o com um especificador de formato.

Por exemplo, gere um arquivo JSON a partir de entradas no diário digitando:

journalctl -b -u nginx -o json
{ "__CURSOR" : "s=13a21661cf4948289c63075db6c25c00;i=116f1;b=81b58db8fd9046ab9f847ddb82a2fa2d;m=19f0daa;t=50e33c33587ae;x=e307daadb4858635", "__REALTIME_TIMESTAMP" : "1422990364739502", "__MONOTONIC_TIMESTAMP" : "27200938", "_BOOT_ID" : "81b58db8fd9046ab9f847ddb82a2fa2d", "PRIORITY" : "6", "_UID" : "0", "_GID" : "0", "_CAP_EFFECTIVE" : "3fffffffff", "_MACHINE_ID" : "752737531a9d1a9c1e3cb52a4ab967ee", "_HOSTNAME" : "desktop", "SYSLOG_FACILITY" : "3", "CODE_FILE" : "src/core/unit.c", "CODE_LINE" : "1402", "CODE_FUNCTION" : "unit_status_log_starting_stopping_reloading", "SYSLOG_IDENTIFIER" : "systemd", "MESSAGE_ID" : "7d4958e842da4a758f6c1cdc7b36dcc5", "_TRANSPORT" : "journal", "_PID" : "1", "_COMM" : "systemd", "_EXE" : "/usr/lib/systemd/systemd", "_CMDLINE" : "/usr/lib/systemd/systemd", "_SYSTEMD_CGROUP" : "/", "UNIT" : "nginx.service", "MESSAGE" : "Starting A high performance web server and a reverse proxy server...", "_SOURCE_REALTIME_TIMESTAMP" : "1422990364737973" }

. . .
Isso é útil para análise com utilitários. Você poderia usar o formato json-pretty para ter uma melhor ideia melhor da estrutura de dados antes de passá-lo para o consumidor de JSON:

journalctl -b -u nginx -o json-pretty
{
	"__CURSOR" : "s=13a21661cf4948289c63075db6c25c00;i=116f1;b=81b58db8fd9046ab9f847ddb82a2fa2d;m=19f0daa;t=50e33c33587ae;x=e307daadb4858635",
	"__REALTIME_TIMESTAMP" : "1422990364739502",
	"__MONOTONIC_TIMESTAMP" : "27200938",
	"_BOOT_ID" : "81b58db8fd9046ab9f847ddb82a2fa2d",
	"PRIORITY" : "6",
	"_UID" : "0",
	"_GID" : "0",
	"_CAP_EFFECTIVE" : "3fffffffff",
	"_MACHINE_ID" : "752737531a9d1a9c1e3cb52a4ab967ee",
	"_HOSTNAME" : "desktop",
	"SYSLOG_FACILITY" : "3",
	"CODE_FILE" : "src/core/unit.c",
	"CODE_LINE" : "1402",
	"CODE_FUNCTION" : "unit_status_log_starting_stopping_reloading",
	"SYSLOG_IDENTIFIER" : "systemd",
	"MESSAGE_ID" : "7d4958e842da4a758f6c1cdc7b36dcc5",
	"_TRANSPORT" : "journal",
	"_PID" : "1",
	"_COMM" : "systemd",
	"_EXE" : "/usr/lib/systemd/systemd",
	"_CMDLINE" : "/usr/lib/systemd/systemd",
	"_SYSTEMD_CGROUP" : "/",
	"UNIT" : "nginx.service",
	"MESSAGE" : "Starting A high performance web server and a reverse proxy server...",
	"_SOURCE_REALTIME_TIMESTAMP" : "1422990364737973"
}

. . .
Os formatos a seguir podem ser usados para exibição:

cat: exibe apenas o campo de mensagem em si.
export: um formato binário adequado para transferir e fazer um backup.
json: JSON padrão com uma entrada por linha.
json-pretty: JSON formatado para uma melhor legibilidade humana
json-sse: saída formatada em JSON agrupada para tornar um evento enviado ao servidor compatível
short: o estilo de saída padrão do syslog
short-iso: o formato padrão aumentado para mostrar as carimbos de data/hora da ISO 8601.
short-monotonic: o formato padrão com carimbos de data/hora monotônicos.
short-precise: o formato padrão com precisão de microssegundos
verbose: exibe todas os campos de diário disponíveis para a entrada, incluindo aqueles que geralmente estão escondidos internamente.
Essas opções permitem exibir as entradas do diário no formato que melhor atende às suas necessidades atuais.

Monitoramento de processo ativo
O comando journalctl imita a forma como muitos administradores usam tail para monitorar atividades ativas ou recentes. Essa funcionalidade está embutida no journalctl, permitindo acessar esses recursos sem precisar utilizar uma outra ferramenta.

Exibir registros recentes
Para exibir uma quantidade definida de registros, use a opção -n, que funciona exatamente como tail -n.

Por padrão, ele irá exibir as 10 entradas mais recentes:

journalctl -n
Especifique o número de entradas que quer ver com um número após o -n:

journalctl -n 20
Acompanhar registros
Para acompanhar ativamente os registros à medida que são escritos, use o sinalizador -f. Novamente, isso funciona como o esperado, caso você tenha experiência usando o tail -f:

journalctl -f
Manutenção do diário
Você deve estar se perguntando sobre o custo do armazenamento de todos os dados que vimos até agora. Além disso, você pode estar interessado em limpar alguns registros mais antigos e liberar o espaço.

Descobrir o uso atual em disco
É possível descobrir a quantidade de espaço que diário está ocupando no disco usando o sinalizador --disk-usage:

journalctl --disk-usage
Journals take up 8.0M on disk.
Deletar registros antigos
Se quiser reduzir o tamanho do seu diário, você pode fazer isso de duas maneiras diferentes (disponível com a versão 218 do systemd ou superior).

Usando a opção --vacuum-size, você pode reduzir o tamanho do diário indicando um tamanho. Isso irá remover entradas antigas até o espaço total que o diário ocupa em disco esteja no tamanho solicitado:

sudo journalctl --vacuum-size=1G
Outra maneira para reduzir o tamanho do diário é fornecendo um tempo de corte com a opção --vacuum-time. Todas as entradas além daquele momento são excluídas. Isso permite manter as entradas que foram criadas após um tempo específico.

Por exemplo, para manter as entradas do último ano, digite:

sudo journalctl --vacuum-time=1years
Limitar a expansão do diário
É possível configurar seu servidor para colocar limites sobre a quantidade de espaço que diário pode ocupar. Isso pode ser feito editando o arquivo /etc/systemd/journald.conf.

Os itens a seguir podem ser usados para limitar o crescimento do diário:

SystemMaxUse=: especifica o espaço máximo em disco que pode ser usado pelo diário em armazenamento persistente.
SystemKeepFree=: especifica a quantidade de espaço que o diário deve deixar livre no armazenamento persistente ao adicionar entradas.
SystemMaxFileSize=: controla o tamanho máximo até o qual os arquivos de diário individuais podem crescer em armazenamento persistente antes de serem girados.
RuntimeMaxUse=: especifica o espaço máximo em disco que pode ser usado como armazenamento volátil (dentro do sistema de arquivos /run).
RuntimeKeepFree=: especifica a quantidade de espaço a ser reservada para outros usos ao escrever dados no armazenamento volátil (dentro do sistema de arquivos /run).
RuntimeMaxFileSize=: especifica a quantidade de espaço que um arquivo de diário individual pode ocupar em armazenamento volátil (dentro do sistema de arquivos /run) antes de ser girado.
Ao definir esses valores, você pode controlar como o journald consome e preserva o espaço no seu servidor. Tenha em mente que o SystemMaxFileSize e o RuntimeMaxFileSize irão mirar em arquivos arquivados para alcançar os limites declarados. Isso é importante de lembrar ao interpretar contagens de arquivos após uma operação de limpeza.

Conclusão
Como foi visto, o diário do systemd é incrivelmente útil para coletar e gerenciar seus dados de sistema e de aplicativo. A maior parte da sua flexibilidade vem da grande quantidade de metadados que são registrados automaticamente e da natureza centralizada do registro. O comando journalctl torna mais fácil aproveitar as funcionalidades avançadas do diário e fazer análises extensas e depuração relacional de diferentes componentes de aplicativos.